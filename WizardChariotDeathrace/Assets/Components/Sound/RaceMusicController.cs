﻿using UnityEngine;
using System.Collections;

public class RaceMusicController : MonoBehaviour 
{

	public AudioClip introMusic;
	public AudioClip backgroundMusic;

	private AudioSource introSource;
	private AudioSource backgroundSource;

	private bool songStarted;

	AudioSource addAudio(AudioClip clip, bool loop)
	{
		AudioSource newAudio = (AudioSource)gameObject.AddComponent (typeof(AudioSource));
		newAudio.clip = clip;
		newAudio.loop = loop;
		newAudio.playOnAwake = false;
		newAudio.volume = 1;
		return newAudio;
	}

	// Use this for initialization
	void Start () 
	{
		songStarted = false;

		introSource = addAudio (introMusic, false);
		backgroundSource = addAudio (backgroundMusic, true);

		introSource.Play ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (songStarted == false) 
		{
			if (!introSource.isPlaying)
			{
				songStarted = true;
				backgroundSource.Play ();
			}
		}
	}
}
