﻿using UnityEngine;
using System.Collections;

public class CheckpointArrow : MonoBehaviour 
{

	public GameObject player;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		float dx = Checkpoint.activeCheckpoint.gameObject.transform.position.x - player.transform.position.x;
		float dy = Checkpoint.activeCheckpoint.gameObject.transform.position.y - player.transform.position.y;

		float dist = Mathf.Sqrt (Mathf.Pow (dx, 2) + Mathf.Pow (dy, 2));

		gameObject.transform.position = new Vector2 (player.transform.position.x + dx / dist * 1.5f, player.transform.position.y + dy / dist * 1.5f);
		gameObject.transform.rotation = Quaternion.Euler (new Vector3 (0, 0, Mathf.Atan2 (dy, dx) * Mathf.Rad2Deg));
	}
}
