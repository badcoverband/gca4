﻿using UnityEngine;
using System.Collections;

public class Wizard : MonoBehaviour {
	Character character;
	Animator animator;

	// Use this for initialization
	void Start () {
		character = transform.parent.gameObject.GetComponent<Character>();
		animator = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		transform.rotation = Quaternion.Euler(0f, 0f, Object2D.VecToDeg(character.inputState.target - character.position));
		animator.SetBool("firing", character.inputState.toFire);
	}
}
