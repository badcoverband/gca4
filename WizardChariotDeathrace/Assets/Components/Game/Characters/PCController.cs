﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Chariot))]
public class PCController : MonoBehaviour {
	Chariot.InputState inputState;

	public GameObject circle;
	GameObject circleClone = null;

	void Start()
	{
		inputState = (Chariot.InputState)this.GetComponent<Chariot>().inputState;
		circle = Resources.Load<GameObject>("Magic Circle");
	}

	void Update()
	{
		if(inputState == null)
		{
			inputState = (Chariot.InputState)this.GetComponent<Chariot>().inputState;
		}

		if(circleClone != null)
		{
			circleClone.transform.position = this.transform.position;
			if(circleClone.transform.localScale.x < 1.0f)
			{
				circleClone.transform.localScale += new Vector3(0.25f, 0.25f, 0.0f);
			}
		}
			
		inputState.turn = -Input.GetAxis("Horizontal");

		Vector3 m = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		inputState.target = new Vector3(m.x, m.y);
		
		if(Input.GetKey(KeyCode.Space))
			inputState.shake = true;
		
		if(Input.GetKey(KeyCode.Alpha1))
			inputState.selectedAbility = 0;
		
		if(Input.GetKey(KeyCode.Alpha2))
			inputState.selectedAbility = 1;
		
		if(Input.GetKey(KeyCode.Alpha3))
			inputState.selectedAbility = 2;
		
		if(Input.GetKey(KeyCode.Alpha4))
			inputState.selectedAbility = 3;
		
		if(Input.GetMouseButtonDown(0)) {
			inputState.toFire = true;
		}
		
		if(Input.GetMouseButtonUp(0)) {
			inputState.toFire = false;
		}
	}

	void OnGUI()
	{
		//GUI Label(new Rect(10,200,100,20), debugVecPoint.ToString ());
		//GUI.Label(new Rect(100,200,100,20), debugVecPlayer.ToString ());
	}
}
