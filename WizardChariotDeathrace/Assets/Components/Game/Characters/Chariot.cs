﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody2D))]
[RequireComponent (typeof(Collider2D))]
public class Chariot : Character {

	float heathBarlength;

	[System.Serializable]
	public class Movement {
		public float maxSpeed = 10.0f;
		public float desiredSpeed = 3.0f;
		public float acceleration = 2.0f;
		public float decceleration = 1.0f;
		public float slideDecceleration = 5f;

		public float minDesiredSpeed = 3.0f;
		public float maxDesiredSpeed = 8.0f;
		public float boostAccel = 1.5f;
		public float decayDelay = 5.0f;
		public float decayDelayCounter = 0.0f;
		public float decayRate = 0.1f;

	}
	public Movement movement = new Movement();

	[System.Serializable]
	public class Steering {
		public float maxTurnSpeed = 60.0f;
		public float desiredTurnVelocity = 0.0f;
		public float turnAcceleration = 180.0f;
		public float slideDecceleration = 235.0f;
	}
	public Steering steering = new Steering();

	[System.Serializable]
	public new class InputState : Character.InputState
	{
		public float turn = 1.0f;
		public bool shake = false;
	}

	//public InputState inputState = new InputState();

	// Use this for initialization
	protected virtual void Start () {
		rigidbody2D.velocity = forward * 1.0f;
		inputState = new InputState();
		abilities = new Ability[4];
		abilities[0] = new MagicMissile();
		abilities[1] = new Flamethrower();
		abilities[2] = new SlipUp();
		abilities[3] = new Bolt();
	}

	// Update is called once per frame
	protected virtual void FixedUpdate () {
		//shake
		if(((InputState)inputState).shake)
		{
			movement.desiredSpeed += movement.boostAccel;
			movement.decayDelayCounter = movement.decayDelay;
			((InputState)inputState).shake = false;
		}

		//update interface

		//step
		movement.decayDelayCounter -= Time.fixedDeltaTime;
		if(movement.decayDelayCounter < 0 && movement.desiredSpeed > movement.minDesiredSpeed)
		{
			movement.desiredSpeed -= movement.decayRate;
		}
		movement.desiredSpeed = Mathf.Clamp(movement.desiredSpeed, movement.minDesiredSpeed, movement.maxDesiredSpeed);
		
		if(rigidbody2D.velocity.sqrMagnitude <= movement.maxSpeed * movement.maxSpeed && 
		   	Mathf.Abs(rigidbody2D.angularVelocity) <= steering.maxTurnSpeed) {
			//float angAccel = (steering.desiredTurnVelocity - rigidbody2D.angularVelocity) / Time.fixedDeltaTime;

			if(inControl)
			{
				float angAccel = (((InputState)inputState).turn * 90.0f - rigidbody2D.angularVelocity) / Time.fixedDeltaTime;
				angAccel = Mathf.Clamp(angAccel, -steering.turnAcceleration, steering.turnAcceleration);
				rigidbody2D.AddTorque(angAccel * Mathf.Deg2Rad * rigidbody2D.mass);
			}

			float speed = Vector2.Dot(rigidbody2D.velocity, forward);
			float accel = (movement.desiredSpeed - speed) / Time.fixedDeltaTime;
			//float accel = (((InputState)inputState).speed - speed) / Time.fixedDeltaTime;
			accel = Mathf.Clamp(accel, -movement.decceleration, movement.acceleration);
			rigidbody2D.AddForce(accel * forward * rigidbody2D.mass);
		
			float slideSpeed = Vector2.Dot(rigidbody2D.velocity, right);
			float slideDeccel = (0f - slideSpeed) / Time.fixedDeltaTime;
			slideDeccel = Mathf.Clamp(slideDeccel, -movement.slideDecceleration, movement.slideDecceleration);
			rigidbody2D.AddForce(slideDeccel * right * rigidbody2D.mass);
		} else {
			rigidbody2D.AddForce(-movement.slideDecceleration * rigidbody2D.velocity.normalized * rigidbody2D.mass);
			rigidbody2D.AddTorque(-Mathf.Sign(rigidbody2D.angularVelocity) * steering.slideDecceleration * Mathf.Deg2Rad * rigidbody2D.mass);
		}
	}

	public override void Update()
	{
		base.Update();
	}

	void OnGUI() {
		if(gameObject.tag == "Player")
		{
			heathBarlength = (Screen.width / 2) * (curHealth / (float)maxHealth);

			GUI.backgroundColor = Color.green;
			GUI.Button(new Rect(10, 10, heathBarlength, 20), curHealth + "/" + maxHealth);
			GUI.Box(new Rect(10, 40, 120, 20), "Score: " + Game.score.ToString());
			//GUI.Label(new Rect(0,0,100,20), health.ToString());
			//GUI.Label(new Rect(10,50,100,20), ((InputState)inputState).selectedAbility.ToString ());
			//GUI.Label(new Rect(10,100,100,20), ((InputState)inputState).turn.ToString ());
			//GUI.Label(new Rect(10,150,100,20), movement.desiredSpeed.ToString ());
		}
	}
}
