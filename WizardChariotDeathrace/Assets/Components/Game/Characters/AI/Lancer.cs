﻿using UnityEngine;
using System.Collections;

public class Lancer : Cavalry {
	public bool isCharging = true;

	[System.Serializable]
	public class Charging {
		public Object2D target;
		public float desiredLead = 15f;
		public float desiredLeadMargin = 2f;
		public float desiredLeadSlowMargin = 2f;
		public float attackWindowWidth = 10f;
		public float gallopTurnAngle = 10.0f;
		public float trotTurnAngle = 90.0f;
		public float speedDamageThreshold = 5.0f;
		public float speedDamageAboveThreshold = 25.0f;
	}
	public Charging charging = new Charging();
	
	// Update is called once per frame
	protected override void FixedUpdate () {
		Vector2 desiredPosition = position;
		
		if(!isCharging) {
			desiredPosition = charging.target.position + charging.target.forward * charging.desiredLead;
			if((desiredPosition - position).sqrMagnitude <= charging.desiredLeadMargin * charging.desiredLeadMargin)
				isCharging = true;
		}

		if(isCharging) {
			Vector2 difference = charging.target.position - position;
			if(Vector2.Dot(difference, charging.target.forward) > 0)
				isCharging = false;
			else if(Mathf.Abs(Vector2.Dot(difference, charging.target.right)) > charging.attackWindowWidth)
				isCharging = false;
			else
				desiredPosition = charging.target.position;
		}
		
		float desiredAngle = VecToDeg(desiredPosition - position);

		float direction = desiredAngle - angle;
		while(direction > 180f)
			direction -= 360f;
		while(direction < -180f)
			direction += 360f;
		
		//hack to avoid player
		if(!isCharging && Mathf.Abs(direction) < 45.0f && (charging.target.position - position).sqrMagnitude < 64f) {
			float targetAngle = VecToDeg(charging.target.position - position);
			
			float targetDirection = desiredAngle - targetAngle;
			while(targetDirection > 180f)
				targetDirection -= 360f;
			while(targetDirection < -180f)
				targetDirection += 360f;
			if(Mathf.Abs(targetDirection) < 45.0f) {
				direction += Mathf.Sign(targetDirection) * 90.0f;
				if(direction > 180f)
					direction -= 360f;
				if(direction < -180f)
					direction += 360f;
			}
		}

		inputState.turn = Mathf.Sign(direction);
		if(Mathf.Abs(direction) > charging.trotTurnAngle) {
			movement.desiredSpeed = movement.trotSpeed;
		} else if(Mathf.Abs(direction) < charging.gallopTurnAngle) {
			movement.desiredSpeed = movement.gallopSpeed;
		} else {
			float trotFactor = (Mathf.Abs(direction) - charging.gallopTurnAngle) / (charging.trotTurnAngle - charging.gallopTurnAngle);
			movement.desiredSpeed = movement.trotSpeed * trotFactor + movement.gallopSpeed * (1 - trotFactor);
		}

		if(!isCharging && (desiredPosition - position).sqrMagnitude <= charging.desiredLeadSlowMargin * charging.desiredLeadSlowMargin)
			movement.desiredSpeed *= 0.75f;

		Debug.DrawLine(position, desiredPosition);
		base.FixedUpdate();
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if(collision.gameObject.tag == "Player") {
			if(collision.relativeVelocity.magnitude >= charging.speedDamageThreshold) {
				collision.gameObject.GetComponent<Character>()
					.Damage(Mathf.RoundToInt((collision.relativeVelocity.magnitude - charging.speedDamageThreshold)
					                         * charging.speedDamageAboveThreshold), DamageType.Physical);
			}
			isCharging = false;
		}
	}
}
