﻿using UnityEngine;
using System.Collections;

public class Chaser : Chariot {

	[System.Serializable]
	public class Tracking {
		public Object2D target;
		public float desiredDistance = 3f;
		public float slowSpeed = 4f;
		public float mediumSpeed = 5f;
		public float fastSpeed = 6.5f;
		public float slowTurnFactor = 0.5f;
		public float mediumTurnFactor = 1.0f;
		public float fastTurnFactor = 1.25f;
		public float closeAngle = 30f;
		public float farAngle = 60f;
	}

	public Tracking tracking = new Tracking();
	public float shotCooldown = 0.0f;

	// Update is called once per frame
	protected override void FixedUpdate() {

		shotCooldown -= Time.fixedDeltaTime;

		if (shotCooldown <= 0) 
		{
			inputState.target = tracking.target.position;
			inputState.toFire = true;
			shotCooldown = Random.Range(20,40)/10.0f;
		}

		//if(Vector2.Distance(position, tracking.target.position) <= 50.0f) {
			movement.decayDelayCounter = float.PositiveInfinity;
			movement.minDesiredSpeed = tracking.slowSpeed;
			movement.maxDesiredSpeed = tracking.fastSpeed;

			Vector2 offsetDirection = (position - tracking.target.position).normalized;
			Vector2 desiredPosition = tracking.target.position +  offsetDirection * tracking.desiredDistance;
			Debug.DrawLine(position, desiredPosition);

			float desiredAngle = VecToDeg(desiredPosition - position);
			float direction = desiredAngle - angle;
			while(direction > 180f)
				direction -= 360f;
			while(direction < -180f)
				direction += 360f;

			if(Mathf.Abs(direction) > tracking.farAngle) {
				((InputState)inputState).turn = tracking.fastTurnFactor * Mathf.Sign(direction);
				movement.desiredSpeed = tracking.slowSpeed;
			} else if(Mathf.Abs(direction) < tracking.closeAngle) {
				((InputState)inputState).turn = tracking.slowTurnFactor * Mathf.Sign(direction);
				movement.desiredSpeed = tracking.fastSpeed;
			} else {
				((InputState)inputState).turn = tracking.mediumTurnFactor * Mathf.Sign(direction);
				movement.desiredSpeed = tracking.mediumSpeed;
			}
			Debug.DrawLine(position, desiredPosition);
		/*} else {
			movement.desiredSpeed = 0.0f;
		}*/
		base.FixedUpdate();
	}
}
