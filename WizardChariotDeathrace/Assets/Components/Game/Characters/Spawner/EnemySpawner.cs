﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour 
{
	public static int enemiesAlive = 0;

	GameObject chaser;
	GameObject lancer;

	GameObject player;

	void Start()
	{
		chaser = Resources.Load<GameObject>("Chaser");
		lancer = Resources.Load<GameObject>("Lancer");
	}

	// Update is called once per frame
	void Update () 
	{

		if (enemiesAlive - 1 <= Game.score / 100) 
		{
			Vector2 spawnPosition = new Vector2(Random.Range (gameObject.transform.position.x - 50, gameObject.transform.position.x + 50),Random.Range (gameObject.transform.position.y - 50,gameObject.transform.position.y + 50));
			int rEnemy = Random.Range (0,2);

			GameObject proClone;

			if (rEnemy == 0)
			{
				proClone = (GameObject)Object.Instantiate(chaser, spawnPosition, Quaternion.Euler(Vector3.forward));
				Chaser temp = (Chaser)proClone.GetComponent (typeof(Chaser));
				temp.tracking.target = (Character)gameObject.GetComponent (typeof(Character));
			}
			else if (rEnemy == 1)
			{
				proClone = (GameObject)Object.Instantiate(lancer, spawnPosition, Quaternion.Euler(Vector3.forward));
				Lancer temp = (Lancer)proClone.GetComponent (typeof(Lancer));
				temp.charging.target = (Character)gameObject.GetComponent (typeof(Character));
			}

			enemiesAlive++;
		}
	}
}
