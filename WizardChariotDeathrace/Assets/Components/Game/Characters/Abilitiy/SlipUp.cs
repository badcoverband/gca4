﻿using UnityEngine;
using System.Collections;

public class SlipUp : Ability {

	static GameObject iceMine = Resources.Load<GameObject>("Mine Prefab");
	
	public override void Use(Character caster, Vector2 target)
	{
		Quaternion rotation = Quaternion.FromToRotation(Vector3.right, Vector3.up);

		Object.Instantiate(iceMine, target, rotation);

		caster.inputState.toFire = false;
	}
}
