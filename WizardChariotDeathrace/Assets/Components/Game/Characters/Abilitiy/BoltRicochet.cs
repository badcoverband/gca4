﻿using UnityEngine;
using System.Collections;

public class BoltRicochet : Ability {

	static GameObject bigSpark = Resources.Load<GameObject>("Big Bolt Prefab");
	public DamageType damageType = DamageType.Physical;

	public override void Use(Character caster, Vector2 target)
	{
		int i = 0;

		Collider2D[] coll = Physics2D.OverlapCircleAll(caster.transform.position, 10.0f);
		GameObject sparkClone;

		for(i = 0; i<coll.Length; i++)
		{
			if(coll[i].gameObject.tag != "Player" && coll[i].gameObject.GetComponent<Character>() != caster)
			{
				Character enemy = coll[i].gameObject.GetComponent<Character>();

				Vector2 dir = ((Vector2)coll[i].gameObject.transform.position - (Vector2)caster.transform.position).normalized;
				Quaternion rotation = Quaternion.FromToRotation(Vector2.right, dir);

				if(enemy != null)
				{
					enemy.Damage(100, damageType);
					sparkClone = (GameObject)Object.Instantiate(bigSpark, caster.transform.position, rotation);
				}
			}
		}
	}
}
