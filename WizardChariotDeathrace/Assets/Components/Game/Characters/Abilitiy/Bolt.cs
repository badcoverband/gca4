﻿using UnityEngine;
using System.Collections;

public class Bolt : Ability {

	static GameObject littleSpark = Resources.Load<GameObject>("Little Bolt Prefab");
	static GameObject bigSpark = Resources.Load<GameObject>("Big Bolt Prefab");
	public DamageType damageType = DamageType.Physical;
	
	public override void Use(Character caster, Vector2 target)
	{
		Collider2D coll = Physics2D.OverlapCircle(target, 1.0f);

		float shotDis = 1.0f;
		Vector2 dir = (target - (Vector2)caster.transform.position).normalized;
		Quaternion rotation = Quaternion.FromToRotation(Vector2.right, dir);
		Vector2 pos = (Vector2)Position(caster) + dir * shotDis;

		if(coll == null)
		{
			Object.Instantiate(littleSpark, pos, rotation);
		}
		else
		{
			Character enemy = coll.gameObject.GetComponent<Character>();
			if(enemy != null)
			{
				Object.Instantiate(bigSpark, pos, rotation);
				enemy.Damage(200, damageType);
				new BoltRicochet().Use(coll.gameObject.GetComponent<Character>(), Vector2.zero);
			}
		}

		caster.inputState.toFire = false;
	}
}
