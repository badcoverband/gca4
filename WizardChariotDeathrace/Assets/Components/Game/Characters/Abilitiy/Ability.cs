﻿using UnityEngine;
using System.Collections;

public abstract class Ability 
{
	public virtual void Use(Character caster, Vector2 target){}

	protected Vector2 Position(Character caster) {
		Wizard wizard = caster.GetComponentInChildren<Wizard>();
		if(wizard != null) {
			return wizard.transform.position;
		} else {
			return caster.position;
		}
	}
}
