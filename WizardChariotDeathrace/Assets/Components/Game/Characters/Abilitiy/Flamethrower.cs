﻿using UnityEngine;
using System.Collections;

public class Flamethrower : Ability {

	static GameObject flame = Resources.Load<GameObject>("Flame Prefab");

	public override void Use(Character caster, Vector2 target)
	{
		float shotDis = 1.0f;
		Vector2 dir = (target - Position(caster)).normalized;
		Quaternion rotation = Quaternion.FromToRotation(Vector3.right, dir);
		Vector2 pos = Position(caster) + dir * shotDis;
		GameObject flameClone = (GameObject)Object.Instantiate(flame, pos, rotation);
		Flame flameInst = flameClone.GetComponent<Flame>();
		flameInst.owner = caster.gameObject;
		flameInst.addedVelocity = caster.rigidbody2D.velocity;
	}
}
