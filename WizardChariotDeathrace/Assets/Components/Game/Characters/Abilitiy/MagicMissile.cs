﻿using UnityEngine;
using System.Collections;

public class MagicMissile : Ability {

	static GameObject projectile = Resources.Load<GameObject>("Magic Missile Prefab");

	public override void Use(Character caster, Vector2 target)
	{
		float shotDis = 1.0f;
		Vector2 dir = (target - Position(caster)).normalized;
		Quaternion rotation = Quaternion.FromToRotation(Vector2.right, dir);
		Vector2 pos = Position(caster) + dir * shotDis;
		GameObject proClone = (GameObject)Object.Instantiate(projectile, pos, rotation);
		proClone.GetComponent<Projectile>().owner = caster.gameObject;

		caster.inputState.toFire = false;
	}
}
