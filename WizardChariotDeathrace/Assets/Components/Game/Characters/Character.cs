using UnityEngine;
using System.Collections;

public class Character : Object2D {
	public class InputState
	{
		public int selectedAbility = 0;			// Type of ability being fired
		public Vector2 target = Vector2.zero;	// Target in world space
		public bool toFire = false;				// Does the projectile need to be fired?
	}

	public int maxHealth = 1000;
	public int curHealth = 1000;
	public bool isDamagable = true;
	public bool inControl = true;
	public float outOfControlCounter = 0;
	public InputState inputState;
	public Ability[] abilities;

	public int Damage(int damage, DamageType type) {
		int initialHealth = curHealth;
		if(isDamagable)
			curHealth -= damage;
		return initialHealth - curHealth;
	}

	public virtual void Update()
	{
		if(inputState.toFire) 
		{
			abilities[inputState.selectedAbility].Use(this, inputState.target); 
			Wizard wizard = this.GetComponentInChildren<Wizard>();
			if(wizard != null) {
				Animator animator = wizard.GetComponent<Animator>();
				if(animator != null)
					animator.Play("wizard-cast");
			}
		}

		outOfControlCounter -= Time.fixedDeltaTime;

		if(outOfControlCounter < 0)
		{
			inControl = true;
		}

		if(curHealth <= 0)
		{
			Die ();
		}
	}

	public void Die()
	{
		if(gameObject.tag == "Player")
		{
			Application.LoadLevel(5);
		}

		Destroy (gameObject);
		EnemySpawner.enemiesAlive--;
	}
}
