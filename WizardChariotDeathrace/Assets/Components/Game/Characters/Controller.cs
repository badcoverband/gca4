﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Chariot))]
public class Controller : MonoBehaviour {
	Chariot.InputState inputState;

	Vector2 swipeVec = Vector2.zero;
	float angle = 0;
	float magnitude = 0;
	float deadZone = 2;
	int numOptions = 4;
	bool whipping = false;
	bool selecting = false;
	public GameObject circle;
	GameObject circleClone = null;

	void Start()
	{
		inputState = (Chariot.InputState)this.GetComponent<Chariot>().inputState;
		circle = Resources.Load<GameObject>("Magic Circle");
	}

	void Update()
	{
		if(inputState == null)
		{
			inputState = (Chariot.InputState)this.GetComponent<Chariot>().inputState;
		}

		if(circleClone != null)
		{
			circleClone.transform.position = this.transform.position;
			circleClone.transform.rotation = Camera.main.transform.rotation;
			if(circleClone.transform.localScale.x < 1.0f)
			{
				circleClone.transform.localScale += new Vector3(0.25f, 0.25f, 0.0f);
			}
		}

//		if(SystemInfo.deviceType == DeviceType.Handheld) {
			if(Input.touchCount > 0)
			{
				Vector3 p = Camera.main.ScreenToWorldPoint(new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, Camera.main.nearClipPlane));
				
				inputState.target = new Vector3(p.x, p.y);

				if(Input.GetTouch(0).phase == TouchPhase.Began)
				{
					Vector3 tPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);

					//If the sprite is not pressed
					//if (!this.GetComponent<PolygonCollider2D>().OverlapPoint(new Vector2(tPos.x, tPos.y)))
					if (this.collider2D != Physics2D.OverlapCircle(new Vector2(tPos.x, tPos.y), 1.0f))
					{
						inputState.toFire = true;
					}
					else
					{
						selecting = true;

						//Spawn the magic circle
						if(circleClone == null)
						{
							circleClone = (GameObject) Object.Instantiate(circle, this.transform.position, Quaternion.identity);
							circleClone.transform.localScale = new Vector3(0.25f, 0.25f, 0.0f);
						}
					}
				}

				// Swipe the screen to choose an ability
				if(Input.GetTouch(0).phase == TouchPhase.Moved)
				{
					if(selecting)
					{
						swipeVec = new Vector2(Input.GetTouch(0).deltaPosition.x, Input.GetTouch(0).deltaPosition.y);
						angle = Vector2.Angle(swipeVec, Vector2.up);
						if(swipeVec.x > 0)
						{
							angle = 360 - angle;
						}
						magnitude = Vector2.Distance(swipeVec, Vector2.zero);
						
						if(magnitude > deadZone)
						{
							inputState.selectedAbility = (int)Mathf.Round(angle * numOptions/360.0f)%numOptions;
						}
					}
				}

				if(Input.GetTouch(0).phase == TouchPhase.Ended)
				{
					inputState.toFire = false;
					selecting = false;

					//Destroy the magic circle
					if(circleClone != null)
					{
						Destroy (circleClone);
						circleClone = null;
					}
				}
			}

			// Turn according to the accelerator input
			inputState.turn = -Input.acceleration.x;

			Mathf.Clamp(inputState.turn, -1.0f, 1.0f);

			// Boost the speed if the device is shaken
			if(Mathf.Abs(Input.acceleration.z) > 1.2 && !whipping)
			{
				inputState.shake = true;
				whipping = true;
			}
			if(Mathf.Abs(Input.acceleration.z) < 0.2 && whipping) { whipping = false; }	
			
			
/*		} else if(SystemInfo.deviceType == DeviceType.Desktop) {
			
			inputState.turn = -Input.GetAxis("Horizontal");
			
			if(Input.GetKey(KeyCode.Space))
				inputState.shake = true;
			
			if(Input.GetKey(KeyCode.Alpha1))
				inputState.selectedAbility = 0;
			
			if(Input.GetKey(KeyCode.Alpha2))
				inputState.selectedAbility = 1;
			
			if(Input.GetKey(KeyCode.Alpha3))
				inputState.selectedAbility = 2;
			
			if(Input.GetKey(KeyCode.Alpha4))
				inputState.selectedAbility = 3;
			
			if(Input.GetMouseButton(0)) {
				Vector3 m = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				
				inputState.target = new Vector3(m.x, m.y);
				inputState.toFire = true;
			}
		}*/
	}

	void OnGUI()
	{
		//GUI Label(new Rect(10,200,100,20), debugVecPoint.ToString ());
		//GUI.Label(new Rect(100,200,100,20), debugVecPlayer.ToString ());
	}
}
