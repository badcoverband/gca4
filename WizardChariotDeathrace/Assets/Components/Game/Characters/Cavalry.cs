﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody2D))]
[RequireComponent (typeof(Collider2D))]
public class Cavalry : Character {
	
	[System.Serializable]
	public new class InputState : Character.InputState
	{
		public float turn = 0.0f;
	}
	public new InputState inputState {
		get { return (InputState)base.inputState; }
		set { base.inputState = value; }
	}
	
	[System.Serializable]
	public class Movement {
		public float trotSpeed = 2.0f;
		public float gallopSpeed = 10.0f;
		public float desiredSpeed = 2.0f;
		public float acceleration = 3.0f;
		public float decceleration = 5.0f;
		public float slideDecceleration = 20f;
		
	}
	public Movement movement = new Movement();
	
	[System.Serializable]
	public class Steering {
		public float trotSpeedTurnCost = 0.1f / 2.0f; // % per unit/s
		public float gallopSpeedTurnCost = 0.6f / 10.0f; // % per unit/s
		public float maxTurnSpeed = 90.0f;
		public float desiredTurnVelocity = 0.0f;
		public float baseTurnVelocity = 75.0f;
		public float turnAcceleration = 180.0f;
		public float slideDecceleration = 235.0f;
	}
	public Steering steering = new Steering();

	// Use this for initialization
	void Start () {
		inputState = new InputState();
	}
	
	// Update is called once per frame
	protected virtual void FixedUpdate () {
		float speed = rigidbody2D.velocity.magnitude;
		float gallopFraction = Mathf.Clamp((speed - movement.trotSpeed) / 
		                                   (movement.gallopSpeed - movement.trotSpeed),
		                                   0f, 1f);
		float maxTurn = Mathf.Clamp(steering.maxTurnSpeed *
		                            (1f - gallopFraction * speed * steering.gallopSpeedTurnCost - 
		 							(1f - gallopFraction) * speed * steering.trotSpeedTurnCost),
		                            0f, steering.maxTurnSpeed);

		steering.desiredTurnVelocity = Mathf.Clamp(steering.baseTurnVelocity * inputState.turn, -maxTurn, maxTurn);

		if(Mathf.Abs(rigidbody2D.angularVelocity) <= steering.maxTurnSpeed) {
			float angAccel = (steering.desiredTurnVelocity - rigidbody2D.angularVelocity) / Time.fixedDeltaTime;
			angAccel = Mathf.Clamp(angAccel, -steering.turnAcceleration, steering.turnAcceleration);
			rigidbody2D.AddTorque(angAccel * Mathf.Deg2Rad * rigidbody2D.mass);

			float accel = (movement.desiredSpeed - speed) / Time.fixedDeltaTime;
			accel = Mathf.Clamp(accel, -movement.decceleration, movement.acceleration);
			rigidbody2D.AddForce(accel * forward * rigidbody2D.mass);
			
			float slideSpeed = Vector2.Dot(rigidbody2D.velocity, right);
			float slideDeccel = (0f - slideSpeed) / Time.fixedDeltaTime;
			slideDeccel = Mathf.Clamp(slideDeccel, -movement.slideDecceleration, movement.slideDecceleration);
			rigidbody2D.AddForce(slideDeccel * right * rigidbody2D.mass);
		} else {
			rigidbody2D.AddForce(-movement.slideDecceleration * rigidbody2D.velocity.normalized * rigidbody2D.mass);
			rigidbody2D.AddTorque(-Mathf.Sign(rigidbody2D.angularVelocity) * steering.slideDecceleration * Mathf.Deg2Rad * rigidbody2D.mass);
		}
	}
}
