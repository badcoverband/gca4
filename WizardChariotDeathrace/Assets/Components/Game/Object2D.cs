﻿using UnityEngine;
using System.Collections;

public class Object2D : MonoBehaviour {

	//-------------------------------------------//
	// Properties to translate between 2D and 3D //
	//-------------------------------------------//
	
	///Direction object is facing in a top down view as degress (0 is right, 90 is up)
	public float angle {
		get { return VecToDeg(forward); }
		set { forward = DegToVec(value); }
	}
	
	///Direction object is facing in a top down view as radians (0 is right, 90 is up)
	public float angleRad {
		get { return VecToRad(forward); }
		set { forward = RadToVec(value); }
	}

	///Direction object is facing in a top down view as X,Y components
	public Vector2 forward {
		get { return ((Vector2)transform.right).normalized; }
		set { transform.right = value; }
	}
	
	///Direction to objects left in a top down view as X,Y components
	public Vector2 left {
		get { return ((Vector2)transform.up).normalized; }
		set { transform.up = value; }
	}

	///Direction to objects right in a top down view as X,Y components
	public Vector2 right {
		get { return (-(Vector2)transform.up).normalized; }
		set { transform.up = -value; }
	}
	
	///Direction to objects back in a top down view as X,Y components
	public Vector2 backward {
		get { return (-(Vector2)transform.right).normalized; }
		set { transform.right = -value; }
	}

	//Position of object cast in 2D world
	public Vector2 position {
		get { return transform.position; }
		set { transform.position = value; }
	}
	
	//Depth of the object, higher values are drawn under lower values
	public float depth {
		get { return transform.position.z; }
		set { transform.position = new Vector3(transform.position.x, transform.position.y, value); }
	}
	
	//-------------------------------------------//
	// Functions to transform boject in 2D space //
	//-------------------------------------------//

	/// <summary>
	/// Move the specified displacement.
	/// </summary>
	/// <param name="displacement">Displacement.</param>
	public void Move(Vector2 displacement) {
		position += displacement;
	}

	/// <summary>
	/// Move the specified distance in direction.
	/// </summary>
	/// <param name="distance">Distance.</param>
	/// <param name="direction">Direction in degrees.</param>
	public void Move(float distance, float direction) {
		position += new Vector2();
	}

	
	
	//-------------------------------------------//
	// Auxilary 2D functions                     //
	//-------------------------------------------//
	
	public static Vector2 RadToVec(float rad) {
		return new Vector2(Mathf.Cos(rad), Mathf.Sin(rad));
	}

	public static Vector2 DegToVec(float deg) {
		return RadToVec(deg * Mathf.Deg2Rad);
	}
	
	public static float VecToDeg(Vector2 vec) {
		return vec.y >= 0f ? Vector2.Angle(Vector2.right, vec) : 360f - Vector2.Angle(Vector2.right, vec);
	}
	
	public static float VecToRad(Vector2 vec) {
		return VecToDeg(vec) * Mathf.Deg2Rad;
	}

}
