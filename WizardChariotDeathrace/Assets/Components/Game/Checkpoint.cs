﻿using UnityEngine;
using System.Collections;

public class Checkpoint : MonoBehaviour 
{
	static ArrayList checkpoints = new ArrayList();
	public static Checkpoint activeCheckpoint = null;
	private bool isActiveCheckpoint {
		get { return activeCheckpoint == this; }
	}
	private SpriteRenderer sRenderer;

	// Use this for initialization
	void Start () 
	{
		sRenderer = (SpriteRenderer)GetComponent( typeof(SpriteRenderer) );

		if (activeCheckpoint == null) 
		{
			activeCheckpoint = this;
			sRenderer.enabled = true;
		} 
		else 
		{
			sRenderer.enabled = false;
			checkpoints.Add (this);
		}
	}

	// Makes checkpoint active
	void activate()
	{
		activeCheckpoint = this;
		sRenderer.enabled = true;
	}

	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnTriggerEnter2D (Collider2D collider) 
	{
		if (isActiveCheckpoint) 
		{
			if (collider.gameObject.tag.Equals ("Player")) 
			{
				Game.score += 50;
				
				//take a random checkpoint and make it the active one
				int rInt = Random.Range(0,checkpoints.Count);
				Checkpoint tCheckpoint = (Checkpoint)checkpoints[rInt];

				while((tCheckpoint.gameObject.transform.position - gameObject.transform.position).magnitude >= 100)
				{
					rInt = Random.Range(0,checkpoints.Count);
					tCheckpoint = (Checkpoint)checkpoints[rInt];
				}

				tCheckpoint.activate();
				
				//remove it from the list
				checkpoints.Remove (tCheckpoint);
				
				//add this to the checkpoints so it can be a target again
				checkpoints.Add (this);
				
				sRenderer.enabled = false;
			}
		}
	}
}
