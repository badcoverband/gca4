﻿using UnityEngine;
using System.Collections;

public class Spark : Object2D {
	public float speed = 4.0f;
	public int damage = 100;
	public DamageType damageType = DamageType.Physical;
	public Object createOnDestroy = null;
	public Vector3 createOnDestroyOffset = Vector3.zero;
	public Vector3 createOnDestroyRotation = Vector3.zero;
	public GameObject owner = null;
	public float expiry = 0.5f;
	
	// Use this for initialization
	void Start () {

	}
	
	void FixedUpdate () {
		transform.position += (Vector3)forward * speed;

		expiry -= Time.fixedDeltaTime;
		
		if(expiry < 0)
		{
			// Destroy projectile when it expires
			Destroy (gameObject);
		}
	}
}