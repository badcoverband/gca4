﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody2D))]
[RequireComponent (typeof(CircleCollider2D))]
public class Projectile : Object2D {
	public float speed = 15f;
	public int damage = 100;
	public DamageType damageType = DamageType.Physical;
	public Object createOnDestroy = null;
	public Vector3 createOnDestroyOffset = Vector3.zero;
	public Vector3 createOnDestroyRotation = Vector3.zero;
	public GameObject owner = null;
	public float expiry = 3.0f;

	// Use this for initialization
	void Start () {
		rigidbody2D.velocity = speed * forward;
	}

	void FixedUpdate () {
		rigidbody2D.velocity = speed * forward;
		expiry -= Time.fixedDeltaTime;

		if(expiry < 0)
		{
			// Destroy projectile when it expires
			Destroy (gameObject);
		}
	}

	void OnCollisionEnter2D(Collision2D collision) {
		Character character = collision.gameObject.GetComponent<Character>();
		if(character.gameObject != owner)
		{
			if(character != null) {
				character.Damage(damage, damageType);
			}
			if(createOnDestroy != null) {
				Instantiate(createOnDestroy,
				            transform.position + createOnDestroyOffset,
				            transform.rotation * Quaternion.Euler(createOnDestroyRotation));
			}
			// Destroy the projectile
			Destroy (gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		Character character = other.gameObject.GetComponent<Character>();
		if(character != null && character.gameObject != owner)
		{
			if(character != null) {
				character.Damage(damage, damageType);
			}
			if(createOnDestroy != null) {
				Instantiate(createOnDestroy,
				            transform.position + createOnDestroyOffset,
				            transform.rotation * Quaternion.Euler(createOnDestroyRotation));
			}
			// Destroy the projectile
			Destroy (gameObject);
		}
	}
}