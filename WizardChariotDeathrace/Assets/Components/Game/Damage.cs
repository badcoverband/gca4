﻿using UnityEngine;
using System.Collections;

public enum DamageType { Direct, Physical, Fire };
