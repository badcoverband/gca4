﻿using UnityEngine;
using System.Collections;

public class Touch : MonoBehaviour 
{
	string touchString = "Unknown";
	Vector2 swipeVec = Vector2.zero;
	float angle = 0;
	float magnitude = 0;
	float deadZone = 2;
	int numOptions = 8;
	float moveDis = 3;
	float moveAng = 0;
	float test = 0;
	int whipCount = 0;
	bool whipping = false;

	void OnGUI()
	{
		if(Input.touchCount > 0)
		{
			// User has touched the screen
			if(Input.GetTouch(0).phase == TouchPhase.Began)
			{
				//touchString = "Phase: Touch Began";
			}
		
			// User is moving their finger across the screen
			if(Input.GetTouch(0).phase == TouchPhase.Moved)
			{
				swipeVec = new Vector2(Input.GetTouch(0).deltaPosition.x, Input.GetTouch(0).deltaPosition.y);
				angle = Vector2.Angle(swipeVec, Vector2.up);
				if(swipeVec.x > 0)
				{
					angle = 360 - angle;
				}
				magnitude = Vector2.Distance(swipeVec, Vector2.zero);

				if(magnitude > deadZone)
				{
					float i = Mathf.Round(angle * numOptions/360.0f)%numOptions;
					moveAng = i * (360.0f / numOptions);

					transform.position = new Vector2(-Mathf.Sin(moveAng * Mathf.Deg2Rad), Mathf.Cos(moveAng * Mathf.Deg2Rad)) * moveDis;
				}
			}

			// User has stopped touching the screen
			if(Input.GetTouch(0).phase == TouchPhase.Ended)
			{
				//touchString = "Phase: Touch Ended";
			}

			// Debugging information
			GUI.Label(new Rect(10, 40, 400, 400), Input.GetTouch(0).deltaPosition.x.ToString());
			GUI.Label(new Rect(10, 70, 400, 400), Input.GetTouch(0).deltaPosition.y.ToString());
			GUI.Label(new Rect(10, 100, 400, 400), Input.GetTouch(0).deltaTime.ToString());
			GUI.Label(new Rect(10, 130, 400, 400), Input.touchCount.ToString());
		}

		// Debugging information
		GUI.Label(new Rect(10, 10, 400, 100), touchString + " " + whipCount.ToString());
		GUI.Label(new Rect(10, 160, 400, 400), angle.ToString());
		GUI.Label(new Rect(10, 190, 400, 400), test.ToString());
	}

	void Update()
	{
		Vector2 dir = Vector2.zero;
		//dir.x = Input.acceleration.x / 10.0f;
		dir.y = Input.acceleration.y / 10.0f;
		test = Input.acceleration.y;		// Used for debugging
		if(Mathf.Abs(Input.acceleration.z) > 1.5 && !whipping)
		{
			touchString = "Whipica!";
			whipCount++;
			whipping = true;
		}
		if(Mathf.Abs(Input.acceleration.z) < 0.5 && whipping)
		{
			whipping = false;
		}
		if(dir.sqrMagnitude > 1)
		{
			dir.Normalize();
		}
		transform.Translate(dir * 5.0f);
	}

}
