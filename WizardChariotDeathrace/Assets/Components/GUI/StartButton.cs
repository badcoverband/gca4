﻿using UnityEngine;
using System.Collections;

//This is a touch button that changes the current scene.
public class StartButton : TouchButton 
{
	public int levelIndex;

	public override void onPressed()
	{
		Application.LoadLevel (levelIndex);
		if(levelIndex == 4)
		{
			Game.score = 0;
			EnemySpawner.enemiesAlive = 0;
		}
	}
}
