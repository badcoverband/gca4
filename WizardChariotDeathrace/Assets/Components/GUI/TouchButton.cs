﻿using UnityEngine;
using System.Collections;

public class TouchButton : MonoBehaviour 
{

	// This function is called when the button is hit
	public virtual void onPressed()
	{

	}

	// Update is called once per frame
	void Update () 
	{
		if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Began) 
		{
			Vector2 tPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);

			//If the button is pressed
			if (this.collider2D == Physics2D.OverlapPoint(new Vector2(tPos.x, tPos.y)))
			{
				onPressed ();
			}
		}
	}
}
