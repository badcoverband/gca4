﻿using UnityEngine;
using System.Collections;

public class TestButton : TouchButton 
{
	private bool visible = true;

	public override void onPressed()
	{
		if (visible) 
		{
			gameObject.GetComponent<SpriteRenderer> ().enabled = false;
			visible = false;
		} 
		else 
		{
			gameObject.GetComponent<SpriteRenderer> ().enabled = true;
			visible = true;
		}
	}
}
