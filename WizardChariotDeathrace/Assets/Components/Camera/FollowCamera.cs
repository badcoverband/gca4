﻿using UnityEngine;
using System.Collections;

public class FollowCamera : MonoBehaviour {

	public GameObject target;
	public Vector2 targetLocation;
	public float targetRotation;
	public float smoothingTime = 0.2f;
	protected float smoothVel = 0.0f;
	public bool followRotation = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(target != null) {
			targetLocation = target.transform.position;
			targetRotation = target.transform.eulerAngles.z;
		}
		transform.position = targetLocation;
		float rotation = Mathf.SmoothDampAngle(transform.eulerAngles.z + 90, targetRotation, ref smoothVel, smoothingTime);
		transform.rotation = Quaternion.Euler(new Vector3(0,0, rotation - 90));
	}
}
